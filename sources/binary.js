var NI = require('./NI');

/**
 * Creates a new binary object.
 * @param {ArrayBuffer|Uint8Array|number} aSizeOrArray
 * @param {number=} aOffset
 * @param {number=} aLen
 */
function createBinary(aSizeOrArray,aOffset,aLen) {
  return (new cBinary()).init(aSizeOrArray,aOffset,aLen);
}
exports.createBinary = createBinary;

/**
 * Read a file to a buffer, using nodejs's fs module.
 */
function readFileToBinary(fs,fileName,aCallback) {
  fs.stat(fileName, function(statErr, stats) {
    if (statErr) {
      aCallback(statErr);
      return;
    }
    fs.open(fileName, "r", function(openErr, fd) {
      if (openErr) {
        aCallback(openErr);
        return;
      }
      var destBuffer = new Buffer(stats.size);
      fs.read(fd, destBuffer, 0, destBuffer.length, null, function(readErr, bytesRead, buffer) {
        if (readErr) {
          aCallback(readErr);
          return;
        }
        var ab = new ArrayBuffer(buffer.length);
        var view = new Uint8Array(ab);
        for (var i = 0; i < buffer.length; ++i) {
          view[i] = buffer[i];
        }
        aCallback(undefined,createBinary(view));
      });
    });
  });
}
exports.readFileToBinary = readFileToBinary;

/**
 * Write bytes to a file on disk.
 */
function writeBytesToFile(fs,fileName,aByteArray,aOffset,aLen) {
  var src = aByteArray;
  var si = aOffset || 0;
  var len = aLen || src.length;
  var buffer = new Buffer(len);
  for (var i = 0; i < len; ++i, ++si) {
    buffer.writeUInt8(src[si], i);
  }
  return fs.writeFileSync(fileName, buffer);
}
exports.writeBytesToFile = writeBytesToFile;

/**
 * Write a binary to a file on disk.
 */
function writeBinaryToFile(fs,fileName,aBinary,aOffset,aLen) {
  return writeBytesToFile(fs,fileName,aBinary.array,aOffset,aLen);
}
exports.writeBinaryToFile = writeBinaryToFile;

/**
 * Compare two binaries
 */
function cmp(aLeft,aRight,aLen) {
  var lc, li = aLeft.getReadPos(),
      rc, ri = aRight.getReadPos();

  if (!aLen) {
    aLen = Math.min(aLeft.getLength(), aRight.getLength());
  }

  var l = aLeft.array;
  var r = aRight.array;
  for (; aLen-- ; ++li, ++ri) {
    lc = l[li];
    rc = r[ri];
    if (lc != rc) {
      return (lc - rc);
    }
  }

  return 0;
}
exports.cmp = cmp;

/**
 * Find the position of the aSearch binary pattern in aBinary. Returns -1 if not found.
 */
function find(aBinary,aSearch) {
  var prev_rpos = aBinary.getReadPos();
  var i = aBinary.getReadPos();
  var len = aBinary.getLength();
  var slen = aSearch.getLength();
  var found;
  for ( ; i < len; ++i) {
    if (cmp(aBinary,aSearch,slen) === 0) {
      found = true;
      break;
    }
    ++aBinary.rpos;
  }

  aBinary.rpos = prev_rpos;
  return found ? i : -1;
}
exports.find = find;

/**
 * Reads a 16 bits big-endian number from the specified array.
 * @private
 */
var _readUInt16BE = function(array, start) {
  return (array[start]<<8) + array[start+1];
};

/**
 * Reads a 16 bits little-endian number from the specified array.
 * @private
 */
var _readUInt16LE = function(array, start) {
  return (array[start+1]<<8) + array[start];
};

/**
 * Reads a 32 bits big-endian number from the specified array.
 * @private
 */
var _readUInt32BE = function(array, start) {
  // >>> 0 "converts" to unsigned
  return ((array[start]<<24) +
          (array[start+1]<<16) +
          (array[start+2]<<8) +
          array[start+3]) >>> 0;
};

/**
 * Reads a 32 bits little-endian number from the specified array.
 * @private
 */
var _readUInt32LE = function(array, start) {
  // >>> 0 "converts" to unsigned
  return ((array[start+3]<<24) +
          (array[start+2]<<16) +
          (array[start+1]<<8) +
          array[start]) >>> 0;
};

/**
 * Writes a 16 bits big-endian number from the specified array.
 * @private
 */
var _writeUInt16BE = function(array, value, offset) {
  array[offset] = (value & 0xff00)>>8;
  array[offset+1] = value & 0xff;
};

/**
 * Writes a 16 bits little-endian number from the specified array.
 * @private
 */
var _writeUInt16LE = function(array, value, offset) {
  array[offset+1] = (value & 0xff00)>>>8;
  array[offset] = value & 0xff;
};

/**
 * Writes a 32 bits big-endian number from the specified array.
 * @private
 */
var _writeUInt32BE = function(array, value, offset) {
  array[offset] = (value & 0xff000000)>>>24;
  array[offset+1] = (value & 0xff0000)>>>16;
  array[offset+2] = (value & 0xff00)>>>8;
  array[offset+3] = value & 0xff;
};

/**
 * Writes a 32 bits little-endian number from the specified array.
 * @private
 */
var _writeUInt32LE = function(array, value, offset) {
  array[offset+3] = (value & 0xff000000)>>>24;
  array[offset+2] = (value & 0xff0000)>>>16;
  array[offset+1] = (value & 0xff00)>>>8;
  array[offset] = value & 0xff;
};

//======================================================================
// Binary class
//======================================================================
/**
 * Binary view class, wrapper to write/read binary data to an ArrayBuffer.
 * @constructor
 */
var cBinary = function() {};

/**
 * cBinary initializer.
 */
cBinary.prototype.init = function(aSizeOrArrayBuffer, aOffset, aLen) {
  this.wpos = 0;
  this.rpos = 0;
  if (aOffset && aLen) {
    this.array = new Uint8Array(aSizeOrArrayBuffer, aOffset, aLen);
  }
  else if (aSizeOrArrayBuffer instanceof Uint8Array) {
    this.array = aSizeOrArrayBuffer;
  }
  else if (typeof aSizeOrArrayBuffer === "string") {
    this.array = new Uint8Array(aSizeOrArrayBuffer.length);
    this.wUTF8(aSizeOrArrayBuffer,aSizeOrArrayBuffer.length);
    this.wpos = 0;
  }
  else {
    this.array = new Uint8Array(aSizeOrArrayBuffer);
  }
  return this;
};

/**
 * Create a sub view of the binary that points to the same buffer;
 */
cBinary.prototype.subref = function(aOffset,aLen) {
  aOffset = aOffset || 0;
  aLen = aLen || (this.getLength()-aOffset);
  return createBinary(this.getBuffer(),aOffset,aLen);
}

/**
 * Create a copy of a sub view of the binary;
 */
cBinary.prototype.subcopy = function(aOffset,aLen) {
  aOffset = aOffset || 0;
  aLen = aLen || (this.getLength()-aOffset);
  var newBinary = createBinary(aLen);
  newBinary.wBytes(this.getArray(),aOffset,aLen);
  return newBinary;
}


/**
 * Get the length of the binary.
 * @expose
 */
cBinary.prototype.getLength = function() {
  return this.array.length;
};

/**
 * Get the underlying Uint8Array.
 */
cBinary.prototype.getUint8Array = function() {
  return this.array;
};

/**
 * Get the underlying ArrayBuffer.
 */
cBinary.prototype.getBuffer = function() {
  return this.array.buffer;
};

/**
 * Converts to an array.
 */
cBinary.prototype.toArray = function() {
  var i, len = this.getLength();
  var dst = new Array(len);
  var src = this.array;
  for (i = 0; i < len; ++i) {
    dst[i] = src[i];
  }
  return dst;
};

/**
 * Converts to a String representation.
 */
cBinary.prototype.toString = function() {
  var that = this;
  var r = "cBinary(";
  var len = that.getLength();
  r += len;
  r += ")";
  return r;
};

//======================================================================
// Binary class - Writer
//======================================================================
cBinary.prototype.getWritePos = function() {
  return this.wpos;
};

cBinary.prototype.setWritePos = function(aPos) {
  this.wpos = aPos;
};

cBinary.prototype.wSeek = function(aAdvance) {
  this.wpos += aAdvance;
}

/** @expose */
cBinary.prototype.w8 = function(aValue) {
  this.array[this.wpos] = aValue;
  this.wpos += 1;
  return this;
};

cBinary.prototype.wLE16 = function(aValue) {
  _writeUInt16LE(this.array, aValue, this.wpos);
  this.wpos += 2;
  return this;
};

/** @expose */
cBinary.prototype.wBE16 = function(aValue) {
  _writeUInt16BE(this.array, aValue, this.wpos);
  this.wpos += 2;
  return this;
};

cBinary.prototype.wLE32 = function(aValue) {
  _writeUInt32LE(this.array, aValue, this.wpos);
  this.wpos += 4;
  return this;
};

/** @expose */
cBinary.prototype.wBE32 = function(aValue) {
  _writeUInt32BE(this.array, aValue, this.wpos);
  this.wpos += 4;
  return this;
};

/** @expose */
cBinary.prototype.wASCII = function(aValue, aLen) {
  var writeEndZero = (aLen === undefined);
  aLen = aLen || aValue.length;
  for (var i = 0; i < aLen; ++i) {
    var c = aValue.charCodeAt(i);
    this.w8(c & 0xFF);
  }
  if (writeEndZero === true) {
    this.w8(0);
  }
  return this;
};

cBinary.prototype.wUCS2LE = function(aValue, aLen) {
  var writeEndZero = (aLen === undefined);
  aLen = aLen || aValue.length;
  for (var i = 0; i < aLen; ++i) {
    var c = aValue.charCodeAt(i);
    this.wLE16(c & 0xFFFF);
  }
  if (writeEndZero === true) {
    this.wLE16(0);
  }
  return this;
};

cBinary.prototype.wUCS2BE = function(aValue, aLen) {
  var writeEndZero = (aLen === undefined);
  aLen = aLen || aValue.length;
  for (var i = 0; i < aLen; ++i) {
    var c = aValue.charCodeAt(i);
    this.wBE16(c & 0xFFFF);
  }
  if (writeEndZero === true) {
    this.wBE16(0);
  }
  return this;
};

/** @expose */
cBinary.prototype.wUTF8 = function(aValue, aLen, aWriteBOM) {
  var writeEndZero = (aLen === undefined);
  if (aWriteBOM == true) {
    this.w8(0xEF);
    this.w8(0xBB);
    this.w8(0xBF);
  }
  aLen = aLen || aValue.length;
  var c, i;
  for (i = 0; i < aLen; ++i) {
    c = aValue.charCodeAt(i);
    if (c < 128) {
      this.w8(c);
    }
    else if ((c > 127) && (c < 2048)) {
      this.w8((c >> 6) | 192);
      this.w8((c & 63) | 128);
    }
    else {
      this.w8((c >> 12) | 224);
      this.w8(((c >> 6) & 63) | 128);
      this.w8((c & 63) | 128);
    }
  }
  if (writeEndZero) {
    this.w8(0);
  }
  return this;
};

/** @expose */
cBinary.prototype.wHexString = function(aValue, aLen) {
  NI.invariant(aLen !== undefined, "Number of bytes not defined !");
  for (var i = 0; i < aLen; ++i) {
    var hex = aValue.substr(i*2,2);
    var n = NI.stringHex2Byte(hex);
    this.w8(n);
  }
  return this;
};

cBinary.prototype.wBytes = function(aSrc,aSrcOffset,aLen) {
  var dst = this.array;
  var i;
  var si = aSrcOffset || 0;
  var len = aLen || (aSrc.length-si);
  var di = this.wpos;
  for (i = 0; i < len; ++i, ++di, ++si) {
    dst[di] = aSrc[si];
  }
  this.wpos += len;
  return len;
}

cBinary.prototype.wBinary = function(aSrc,aLen) {
  var len = this.wBytes(aSrc.array,aSrc.getReadPos(),aLen);
  aSrc.rSeek(len);
  return len;
}

//======================================================================
// Binary class - Reader
//======================================================================
cBinary.prototype.getReadPos = function() {
  return this.rpos;
};

cBinary.prototype.setReadPos = function(aPos) {
  this.rpos = aPos;
};

cBinary.prototype.rSeek = function(aAdvance) {
  this.rpos += aAdvance;
};

/** @expose */
cBinary.prototype.r8 = function() {
  var v = this.array[this.rpos];
  this.rpos += 1;
  return v;
};

cBinary.prototype.rLE16 = function() {
  var v = _readUInt16LE(this.array, this.rpos);
  this.rpos += 2;
  return v;
};

/** @expose */
cBinary.prototype.rBE16 = function() {
  var v = _readUInt16BE(this.array, this.rpos);
  this.rpos += 2;
  return v;
};

cBinary.prototype.rLE32 = function() {
  var v = _readUInt32LE(this.array, this.rpos);
  this.rpos += 4;
  return v;
};

/** @expose */
cBinary.prototype.rBE32 = function() {
  var v = _readUInt32BE(this.array, this.rpos);
  this.rpos += 4;
  return v;
};

/** @expose */
cBinary.prototype.rASCII = function(aLen) {
  var r = "", c;
  if (aLen != undefined) {
    while (aLen-- > 0) {
      c = this.r8();
      if (c === undefined) {
        break;
      }
      r += String.fromCharCode(c);
    }
    // read to the end
    while (aLen-- > 0) {
      this.r8();
    }
  }
  else {
    for (;;) {
      c = this.r8();
      if (!c) {
        break;
      }
      r += String.fromCharCode(c);
    }
  }
  return r;
};

cBinary.prototype.rUCS2LE = function(aLen) {
  var r = "", c;
  if (aLen != undefined) {
    while (aLen-- > 0) {
      c = this.rLE16();
      if (c === undefined) {
        break;
      }
      r += String.fromCharCode(c);
    }
    // read to the end
    while (aLen-- > 0) {
      this.rLE16();
    }
  }
  else {
    for (;;) {
      c = this.rLE16();
      if (!c) {
        break;
      }
      r += String.fromCharCode(c);
    }
  }
  return r;
};

cBinary.prototype.rUCS2BE = function(aLen) {
  var r = "", c;
  if (aLen != undefined) {
    while (aLen-- > 0) {
      c = this.rBE16();
      if (c === undefined) {
        break;
      }
      r += String.fromCharCode(c);
    }
    // read to the end
    while (aLen-- > 0) {
      this.rBE16();
    }
  }
  else {
    for (;;) {
      c = this.rBE16();
      if (!c) {
        break;
      }
      r += String.fromCharCode(c);
    }
  }
  return r;
};

/** @expose */
cBinary.prototype.rUTF8 = function(aReadBOM) {
  if (aReadBOM) {
    var bom1 = this.r8();
    var bom2 = this.r8();
    var bom3 = this.r8();
    NI.invariant(bom1 === 0xEF && bom2 === 0xBB && bom3 === 0xBF,
                 "Invalid UTF8 BOM");
  }

  var r = "";
  var c = 0;
  var c2 = 0;
  var c3 = 0;
  for (;;) {
    c = this.r8();
    if (!c) {
      break;
    }

    if (c < 128) {
      r += String.fromCharCode(c);
    }
    else if ((c > 191) && (c < 223)) {
      c2 = this.r8();
      r += String.fromCharCode(((c&31) << 6) | (c2&63));
    }
    else {
      c2 = this.r8();
      c3 = this.r8();
      r += String.fromCharCode(((c&15)<<12) | ((c2&63)<<6) | (c3&63));
    }
  }

  return r;
};

/** @expose */
cBinary.prototype.rHexString = function(aLen) {
  aLen = aLen || (this.getLength()-this.getReadPos());
  var r = "";
  for (var i = 0; i < aLen; ++i) {
    var n = this.r8();
    r += NI.stringByte2Hex(n).toUpperCase();
  }
  return r;
};

cBinary.prototype.rBytes = function(aDest,aDestOffset,aLen) {
  var src = this.array;
  var i;
  var si = this.rpos;
  var di = aDestOffset;
  for (i = 0; i < aLen; ++i, ++di, ++si) {
    aDest[di] = src[si];
  }
  this.rpos += aLen;
}

//======================================================================
// Binary class - Properties
//======================================================================
/**
 * Write an object's property.
 */
cBinary.prototype.serializeProperty = function(aWrite,aObj,aProperty,aType,aDefault) {
  NI.invariant(aWrite === true || aWrite === false,
               "Serialize mode not specified !");

  var r;
  var that = this;
  if (typeof aType == "function") {
    // function (aWrite, aDestinationObject, aParentObject - where property is written)
    if (aWrite) {
      var value = (aObj && (aProperty in aObj)) ? aObj[aProperty] : aDefault;
      r = aType.call(that, aWrite, value, aObj);
    }
    else {
      r = aType.call(that, aWrite, {}, aObj);
      if (aObj) {
        aObj[aProperty] = r;
      }
    }
  }
  else {
    var param = undefined;
    if (typeof aType == "object") {
      param = aType[1];
      aType = aType[0];
    }
    var meth = this[(aWrite?"w":"r")+aType];
    if (typeof meth != "function") {
      throw new Error("No "+(aWrite?"w":"r")+" method for type '"+aType+"' !");
    }
    if (aWrite) {
      // Write
      if (aObj && (aProperty in aObj)) {
        var v = aObj[aProperty];
        if (v === undefined) {
          throw new Error("Value of property '"+aProperty+"' is undefined !");
        }
        r = meth.call(that, aObj[aProperty], param);
      }
      else {
        if (aDefault == undefined) {
          throw new Error("No default value for property '"+aProperty+"' !");
        }
        r = meth.call(that, aDefault, param);
      }
    }
    else {
      // Read
      r = meth.call(that, param);
      if (aObj) {
        aObj[aProperty] = r;
      }
    }
  }
  return r;
};

cBinary.prototype.serializeProperties = function(aWrite, aObj, aPropertiesArray) {
  NI.invariant(aWrite === true || aWrite === false,
               "Serialize mode not specified !");

  if (!aWrite && (aObj === undefined)) {
    aObj = {};
  }

  var that = this;
  var numProps = aPropertiesArray.length;
  for (var i = 0; i < numProps; i += 3) {
    var pname = aPropertiesArray[i+0];
    var type  = aPropertiesArray[i+1];
    var value = aPropertiesArray[i+2];
    this.serializeProperty.call(that, aWrite, aObj, pname, type, value);
  }

  return aObj;
};
