var FS = require('fs');
var PATH = require('./path');
var _0777 = parseInt('0777', 8);

FS.canonicalPath = PATH.canonicalPath;

// List all files in a directory in Node.js recursively in a synchronous fashion
var walkSync = function(dir, filelist) {
  dir = PATH.canonicalPath(dir);
  if (dir[dir.length-1] != '/') {
    dir = dir.concat('/')
  }
  var files = FS.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    file = PATH.canonicalPath(file);
    if (FS.statSync(dir + file).isDirectory()) {
      filelist = walkSync(dir + file + '/', filelist);
    }
    else {
      filelist.push(dir+file);
    }
  });
  return filelist;
};
FS.walkSync = walkSync;

function listFiles(logDir, aCallback) {
  // look up the dir for logs
  FS.readdir(logDir, function(err, files) {
    if (err) {
      return aCallback(err);
    }

    var logFiles = [];

    // Fix the Array
    files = Array.prototype.sort.apply(files,[]);

    files.forEach(function(file) {
      var filePath = PATH.join(logDir, file);
      var isFile = FS.statSync(filePath).isFile();
      if(isFile) {
        logFiles.push(file);
      }
    });

    aCallback(null, logFiles);
  });
}
FS.listFiles = listFiles;

function listFilesSync(logDir) {
  var files = FS.readdirSync(logDir)
  var logFiles = [];

  // Fix the Array
  files = Array.prototype.sort.apply(files,[]);

  files.forEach(function(file) {
    var filePath = PATH.join(logDir, file);
    var isFile = FS.statSync(filePath).isFile();
      if(isFile) {
        logFiles.push(file);
      }
  });

  return logFiles;
}
FS.listFilesSync = listFilesSync;

var rmDirSync = function(dirPath, aLog) {
  var ret = true;
  var files;
  try { files = FS.readdirSync(dirPath); }
  catch(e) { return; }
  if (files.length > 0) {
    for (var i = 0; i < files.length; i++) {
      var filePath = dirPath + '/' + files[i];
      if (FS.statSync(filePath).isFile()) {
        if (aLog) {
          aLog("file: " + filePath);
        }
        FS.unlinkSync(filePath);
      }
      else {
        if (aLog) {
          aLog("dir: " + filePath);
        }
        rmDirSync(filePath);
      }
    }
  }
  try {
    FS.rmdirSync(dirPath);
  }
  catch (e) {
    ret = false;
  }
  return ret;
};
FS.rmDirSync = rmDirSync;

var _mkdirSync = FS.mkdirSync;
var mkdirSync = function(path) {
  try {
    _mkdirSync(path);
  } catch(e) {
    if (e.code != 'EEXIST') {
      throw e;
    }
  }
}
FS.mkdirSync = mkdirSync;

function mkdirp(p, opts, f, made) {
  if (typeof opts === 'function') {
    f = opts;
    opts = {};
  }
  else if (!opts || typeof opts !== 'object') {
    opts = { mode: opts };
  }

  var mode = opts.mode;
  var xfs = opts.fs || FS;

  if (mode === undefined) {
    mode = _0777 & (~process.umask());
  }
  if (!made) {
    made = null;
  }

  var cb = f || function () {};
  p = PATH.resolve(p);

  xfs.mkdir(p, mode, function (er) {
    if (!er) {
      made = made || p;
      return cb(null, made);
    }
    switch (er.code) {
    case 'ENOENT':
      mkdirp(PATH.dirname(p), opts, function (erENOENT, madeENOENT) {
        if (erENOENT) {
          cb(erENOENT, madeENOENT);
        }
        else {
          mkdirp(p, opts, cb, madeENOENT);
        }
      });
      break;

      // In the case of any other error, just see if there's a dir
      // there already.  If so, then hooray!  If not, then something
      // is borked.
    default:
      xfs.stat(p, function (er2, stat) {
        // if the stat fails, then that's super weird.
        // let the original error be the failure reason.
        if (er2 || !stat.isDirectory()) {
          cb(er, made)
        }
        else {
          cb(null, made);
        }
      });
      break;
    }
  });
}
FS.mkdirp = mkdirp;

function mkdirpSync(p, opts, made) {
  if (!opts || typeof opts !== 'object') {
    opts = { mode: opts };
  }

  var mode = opts.mode;
  var xfs = opts.fs || FS;

  if (mode === undefined) {
    mode = _0777 & (~process.umask());
  }
  if (!made) {
    made = null;
  }

  p = PATH.resolve(p);
  try {
    xfs.mkdirSync(p, mode);
    made = made || p;
  }
  catch (err0) {
    switch (err0.code) {
    case 'ENOENT' :
      made = mkdirpSync(PATH.dirname(p), opts, made);
      mkdirpSync(p, opts, made);
      break;

      // In the case of any other error, just see if there's a dir
      // there already.  If so, then hooray!  If not, then something
      // is borked.
    default:
      var stat;
      try {
        stat = xfs.statSync(p);
      }
      catch (err1) {
        throw err0;
      }
      if (!stat.isDirectory()) {
        throw err0;
      }
      break;
    }
  }

  return made;
};
FS.mkdirpSync = mkdirpSync;

var __READLINE = null;
function getReadline() {
  if (!__READLINE) {
    __READLINE = require('readline');
  }
  return __READLINE;
}
FS.getReadline = getReadline;

function readLines(aPathOrStream,aOnDone,aOnLine) {
  var rl = getReadline().createInterface({
    input: NI.isString(aPathOrStream) ? FS.createReadStream(aPathOrStream) : aPathOrStream
  });
  rl.on('close', function () {
    aOnDone && aOnDone();
  });
  rl.on('SIGCONT', function () {
    aOnDone && aOnDone('SIGCONT');
  });
  rl.on('SIGINT', function () {
    aOnDone && aOnDone('SIGINT');
  });
  rl.on('SIGSTP', function () {
    aOnDone && aOnDone('SIGSTP');
  });
  rl.on('line', function (aLine) {
    if (aOnLine) {
      if (aOnLine(aLine) === false) {
        // rl.close() doesn't guarantee that no more lines will be read...
        rl.close();
        aOnLine = null;
      }
    }
  });
}
FS.readLines = readLines;

module.exports = FS;
