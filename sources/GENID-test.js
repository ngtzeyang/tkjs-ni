/* global FIXTURE, TEST */
var NI = require('./NI');
var GENID = require('./GENID');

FIXTURE("GENID", function() {
  TEST("print test", function() {
    NI.print("... GENID.makeShortID: " + GENID.makeShortID());

    var b64UUID = GENID.makeUUID();
    NI.print("... GENID.makeUUID:           " + b64UUID);
    NI.print("... NI.b64decodeHex(b64UUID): " + NI.b64decodeHex(b64UUID));
    NI.assert.equals(b64UUID, NI.b64encodeHex(NI.b64decodeHex(b64UUID)));

    var hexUUID = GENID.makeUUIDHex();
    NI.print("... GENID.makeUUIDHex:               " + hexUUID);
    var b64encodedUUID = NI.b64encodeHex(hexUUID);
    NI.print("... NI.b64encodeHex(NI.makeUUIDHex): " + b64encodedUUID);
    NI.print("... NI.b64decodeHex(b64encodedUUID): " + NI.b64decodeHex(b64encodedUUID));
    NI.assert.equals(hexUUID, NI.b64decodeHex(b64encodedUUID));

    var tokenID = GENID.makeTokenID();
    NI.print("... GENID.makeTokenID: " + tokenID);
  })
})
