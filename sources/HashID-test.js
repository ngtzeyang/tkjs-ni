/* global FIXTURE, TEST */
var NI = require('./NI');
var HashID = require('./HashID');

FIXTURE("HashID", function() {
  TEST("hash.encode/decodeNumber", function() {
    function doTest(aType, hash) {
      var number = 123456;
      var encodedNumber = hash.encodeNumber(number);
      NI.println("... " + aType + ", encodedNumber: " + encodedNumber);
      var decodedNumber = hash.decodeNumber(encodedNumber);
      NI.println("... " + aType + ", decodedNumber: " + decodedNumber);
      NI.assert.equals(number, decodedNumber);
    }
    doTest("readable", HashID.createReadable());
    doTest("url safe", HashID.createUrlSafe());
  })

  TEST("hash.encode/decodeString", function() {
    function doTest(aType, hash) {
      var string = "AB123456";
      var encodedString = hash.encodeString(string);
      NI.println("... " + aType + ", encodedString: " + encodedString);
      var decodedString = hash.decodeString(encodedString);
      NI.println("... " + aType + ", decodedString: " + decodedString);
      NI.assert.equals(string, decodedString);

      var string = "A123456789";
      var encodedString = hash.encodeString(string);
      NI.println("... " + aType + ", encodedString: " + encodedString);
      var decodedString = hash.decodeString(encodedString);
      NI.println("... " + aType + ", decodedString: " + decodedString);
      NI.assert.equals(string, decodedString);
    }
    doTest("readable", HashID.createReadable());
    doTest("url safe", HashID.createUrlSafe());
  })

  TEST("hash.encode/decodeHex", function() {
    function doTest(aType, hash) {
      var hex = "a647755229fc64c67f6012493ad12fdcfe364659";
      var encodedHex = hash.encodeHex(hex);
      NI.println("... " + aType + ", encodedHex: " + encodedHex);
      var decodedHex = hash.decodeHex(encodedHex);
      NI.println("... " + aType + ", decodedHex: " + decodedHex);
      NI.assert.equals(hex.toLowerCase(), decodedHex);
    }
    doTest("readable", HashID.createReadable());
    doTest("url safe", HashID.createUrlSafe());
  })
})
