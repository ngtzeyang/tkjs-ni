/// <reference path="typings/all.d.ts" />
/// <reference path="typings/unittest.d.ts" />
var assert = require("assert");
var NI = require('./NI');

FIXTURE("typescript NI.Promise", function() {
  TEST_ASYNC("promise.all", function(aDone) {

    var p1 = Promise.resolve(3);
    var p2 = 1337;
    var p3 = NI.Promise(function(resolve /*, reject*/) {
      setTimeout(resolve, 100, "foo");
    });

    Promise.all([p1, p2, p3]).then(function(values) {
      console.log(values); // [3, 1337, "foo"]
      NI.assert.equals(3, values[0]);
      NI.assert.equals(1337, values[1]);
      NI.assert.equals("foo", values[2]);
      aDone();
    }).catch(aDone);
  })
})
